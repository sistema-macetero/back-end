package com.tfg.maceteros.service.impl;

import com.tfg.maceteros.dto.TimeLineDTO;
import com.tfg.maceteros.mappers.TimelineMapper;
import com.tfg.maceteros.modelo.Sensor;
import com.tfg.maceteros.modelo.dao.SensorDao;
import com.tfg.maceteros.modelo.dao.TimeLineDao;
import com.tfg.maceteros.service.ITimeLineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TimeLineServiceImpl implements ITimeLineService {
    @Autowired
    private SensorDao sensorDao;
    @Autowired
    private TimeLineDao timeLineDao;
    @Autowired
    private TimelineMapper timelineMapper;

    public List<TimeLineDTO> valoresSensor(String sensor_id){
        Optional<Sensor> sensor = sensorDao.findById(Long.parseLong(sensor_id));
        return sensor.isPresent() ? timelineMapper.entitiesToDto(timeLineDao.findBySensor(sensor.get())) : null;
    }
}
