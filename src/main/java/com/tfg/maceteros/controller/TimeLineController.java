package com.tfg.maceteros.controller;

import com.tfg.maceteros.dto.TimeLineDTO;
import com.tfg.maceteros.service.ITimeLineService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api/timeLine")
@Api(tags = "Timeline Controller")
public class TimeLineController {

    @Autowired
    private ITimeLineService service;

    @GetMapping(value = "/{sensor_id}")
    @ApiOperation(value = "Operación para listar los 100 ultimos  valores recogidos por un sensor")
    public List<TimeLineDTO> valoresSensor(@PathVariable(value = "sensor_id") @ApiParam(value = "Identificador del sensor a buscar") String sensorId ) {
        return service.valoresSensor(sensorId);
    }
}
